package demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.mycode.ComprasPublicasApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ComprasPublicasApplication.class)
@WebAppConfiguration
public class ComprasPublicasApplicationTests {

	@Test
	public void contextLoads() {
	}

}
