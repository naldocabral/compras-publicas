var appConfig = angular.module('app', ['ngRoute', 'chieffancypants.loadingBar', 'ngAnimate']);
appConfig.config(['$routeProvider', function($routeProvider){
	$routeProvider.
		when('/', {
			templateUrl:'../pages/pageHome.html',
			controller: 'AnoTotalController'
		}).
		when('/agrupar/ano/total', {
			templateUrl:'../pages/pageAnoTotal.html',
			controller: 'AnoTotalController'
		}).
		when('/agrupar/orgao/ano/total', {
			templateUrl:'../pages/pageOrgaoAnoTotal.html',
			controller:'OrgaoAnoTotalController'
		}).
		when('/filtro', {
			templateUrl:'../pages/pageFiltro.html',
			controller:'FiltroController'
		});
}]).
	controller('HomeController', ['$http', function($http){
		
		console.log("HeloController");
		//$http.get('/query/')
	}]).
		controller('FiltroController', ['$http', function($http){
			console.log("FiltroController");
			var self = this;
			
			self.respostaVazia = false;
			self.respostaNaoVazia = false;
			
			self.menu={
					dataInicial:"Selecione o ano inicial",
					dataFinal:"Selecione o ano final",
					orgao:"Selecione um orgao"
			}
			
			self.busca = {}
								
			self.anos = [2010, 2011, 2012, 2013, 2014, 2015];
			
			self.orgaos = [];
			
			self.config = {
					categorias:[],
					series:[]
			};
			
			
			$http.get('/buscaOrgaos').success(function(data){
				self.orgaos = data;
				
			}).error(function(data){
				console.log(data);
			});
			
			self.fornecedores = [];
			
			self.filtrar = function(){
				self.respostaVazia = false;
				self.respostaNaoVazia = false;
				
				$http.get('/filtro?orgao='+self.busca.orgao+'&dataInicial='+self.busca.dataInicial+'&dataFinal='+self.busca.dataFinal+'').success(function(data){
					self.config = data;
					if(self.config.categorias.length == 0){
						self.respostaVazia = true;
					}else{
						self.respostaNaoVazia = true;
					}
					
					
				}).error(function(data){
					console.log(data);
				});
				
				$http.get('filtroFornecedor?orgao='+self.busca.orgao).success(function(data){
					self.fornecedores = data;
					atualizaGrafico();
					/*if(self.fornecedores.length == 0){
						console.log("entrou na condição (FILTRO)");
						self.respostaVazia = true;
					}else{
						self.respostaNaoVazia = true;
					}*/
				
				}).error(function(data){
					console.log(data);
				});
				
			}			
			
			
			self.atualizaOrgao = function(orgao){
				self.busca.orgao = orgao; //para teste
				//self.menu.orgao = orgao;
			}
			
			self.atualizaDataInicial = function(dataInicial){
				self.busca.dataInicial = dataInicial;
				//self.menu.dataInicial = dataInicial;
			}
			
			self.atualizaDataFinal = function(dataFinal){
				self.busca.dataFinal = dataFinal;
				//self.menu.dataFinal = dataFinal;
			}
			
			function atualizaGrafico (){
				//charts
				
				$(function () {
				    $('#orgao').highcharts({
				        chart: {
				            type: 'bar'
				        },
				        title: {
				            text: 'Resumo dos gastos públicos por órgaos federais'
				        },
				        subtitle: {
				            text: 'http://compras.dados.gov.br/'
				        },
				        xAxis: {
				            categories: self.config.categorias,
				            title: {
				                text: null
				            }
				        },
				        yAxis: {
				            min: 0,
				            title: {
				                text: 'Compras (R$)',
				                align: 'high'
				            },
				            labels: {
				                overflow: 'justify'
				            }
				        },
				        tooltip: {
				            valueSuffix: ' REAIS'
				        },
				        plotOptions: {
				            bar: {
				                dataLabels: {
				                    enabled: true
				                }
				            }
				        },
				        legend: {
				            layout: 'vertical',
				            align: 'right',
				            verticalAlign: 'top',
				            x: -40,
				            y: 80,
				            floating: true,
				            borderWidth: 1,
				            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
				            shadow: true
				        },
				        credits: {
				            enabled: false
				        },
				        series: self.config.series //series charts
				    });
				});
				
				$(function () {
				    $('#fornecedores').highcharts({
				        chart: {
				            type: 'column'
				        },
				        title: {
				            text: 'Maiores Fornecedores do órgao: '+self.config.categorias[0]
				        },
				        subtitle: {
				            text: 'Maiores fornecedores do período selecionado'
				        },
				        xAxis: {
				            type: 'category',
				            labels: {
				                rotation: -45,
				                style: {
				                    fontSize: '13px',
				                    fontFamily: 'Verdana, sans-serif'
				                }
				            }
				        },
				        yAxis: {
				            min: 0,
				            title: {
				                text: 'Compras (R$)'
				            }
				        },
				        legend: {
				            enabled: false
				        },
				        tooltip: {
				            pointFormat: 'Gastos: <b>{point.y:1f} </b>'
				        },
				        series: [{
				            name: 'Maiores fornecedores do órgão',
				            data:self.fornecedores,
				            dataLabels: {
				                enabled: true,
				                rotation: -90,
				                color: '#FFFFFF',
				                align: 'right',
				                format: '{point.y:1f}', // one decimal
				                y: 10, // 10 pixels down from the top
				                style: {
				                    fontSize: '13px',
				                    fontFamily: 'Verdana, sans-serif'
				                }
				            }
				        }]
				    });
				});
				
			}//end atualiza grafico
			
			
			
		}]).
		controller('AnoTotalController', ['$http', function($http){
			var self = this;
			self.listView = [];
			self.error = null;
			$http.get('/query/soma/contrato').success(function(data){
				self.listView = data;
				atualizaGrafico(self.listView);
				console.log(self.listView);
			}).error(function(error){
				self.error = error;
			});
			
		var atualizaGrafico = function(lista){
			$(function () {
			    $('#container').highcharts({
			        chart: {
			            type: 'column'
			        },
			        title: {
			            text: 'Evolução dos gastos públicos'
			        },
			        subtitle: {
			            text: 'Um somatório dos gastos governamentais apartir do ano de 2010'
			        },
			        xAxis: {
			            type: 'category',
			            labels: {
			                rotation: -45,
			                style: {
			                    fontSize: '13px',
			                    fontFamily: 'Verdana, sans-serif'
			                }
			            }
			        },
			        yAxis: {
			            min: 0,
			            title: {
			                text: 'Gastos (R$)'
			            }
			        },
			        legend: {
			            enabled: false
			        },
			        tooltip: {
			            pointFormat: 'Gastos: <b>{point.y:.1f} milhões</b>'
			        },
			        series: [{
			            name: 'Gastos públicos',
			            data: [[lista[2][0]+"", lista[2][1]],
			                   [lista[3][0]+"", lista[3][1]],
			                   [lista[4][0]+"", lista[4][1]],
			                   [lista[5][0]+"", lista[5][1]],
			                   [lista[6][0]+"", lista[6][1]]
			                   ],
			            dataLabels: {
			                enabled: true,
			                rotation: -90,
			                color: '#FFFFFF',
			                align: 'right',
			                format: '{point.y:.1f}', // one decimal
			                y: 10, // 10 pixels down from the top
			                style: {
			                    fontSize: '13px',
			                    fontFamily: 'Verdana, sans-serif'
			                }
			            }
			        }]
			    });
			});
			
			//end atualiza grafico
		}	    		      
		
		//end Controller
		
			console.log("entrou no ano total controller");
		}]).
		controller('OrgaoAnoTotalController', ['$http', function($http){
			console.log("entrou no Orgao ano total controller");
			
			var self = this;
			self.config = {};
			
			$http.get('/query/agrupar/orgao/ano/total').success(function(data){
				console.log(data);
				self.config = data;
				atualizaGrafico();
				
			});
			
			function atualizaGrafico (){
				//charts
				$(function () {
				    $('#container').highcharts({
				        chart: {
				            type: 'bar'
				        },
				        title: {
				            text: 'Resumo dos gastos públicos por órgaos federais'
				        },
				        subtitle: {
				            text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
				        },
				        xAxis: {
				            categories: self.config.categorias,
				            title: {
				                text: null
				            }
				        },
				        yAxis: {
				            min: 0,
				            title: {
				                text: 'Population (millions)',
				                align: 'high'
				            },
				            labels: {
				                overflow: 'justify'
				            }
				        },
				        tooltip: {
				            valueSuffix: ' millions'
				        },
				        plotOptions: {
				            bar: {
				                dataLabels: {
				                    enabled: true
				                }
				            }
				        },
				        legend: {
				            layout: 'vertical',
				            align: 'right',
				            verticalAlign: 'top',
				            x: -40,
				            y: 80,
				            floating: true,
				            borderWidth: 1,
				            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
				            shadow: true
				        },
				        credits: {
				            enabled: false
				        },
				        series: self.config.series //series charts
				    });
				});

				//end chart

			} 
			
		}]);