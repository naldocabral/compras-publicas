package br.com.mycode.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.mycode.entidades.TipoDeContrato;

public interface TipoDeContratoRepository extends
		JpaRepository<TipoDeContrato, Long> {

	@Query(nativeQuery = true, value = "select * from tipo_de_contrato where id=?1 ")
	TipoDeContrato findOne(Long tipo_contrato);

}
