package br.com.mycode.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.mycode.entidades.NaturezaJuridica;

public interface NaturezaJuridicaRepository extends
		JpaRepository<NaturezaJuridica, Long> {

}
