package br.com.mycode.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.mycode.entidades.ModalidadeLicitacao;

public interface ModalidadeLicitacaoRepository extends
		JpaRepository<ModalidadeLicitacao, Long> {

	@Query(nativeQuery = true, value = "select * from modalidade_licitacao where id=?1")
	ModalidadeLicitacao findOne(Long modalidade_licit);

}
