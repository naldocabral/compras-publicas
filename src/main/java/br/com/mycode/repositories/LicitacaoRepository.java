package br.com.mycode.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.mycode.entidades.Licitacao;

public interface LicitacaoRepository extends JpaRepository<Licitacao, Long> {

	@Query(nativeQuery = true, value = "select * from licitacao where identificador=?1")
	Licitacao findOne(String licitacao_associada);

	@Query(nativeQuery=true, value="select * from licitacao where uasg_falsa=?1")
	List<Licitacao> findAll(String id);

}
