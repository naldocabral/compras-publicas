package br.com.mycode.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.mycode.entidades.Fornecedor;

public interface FornecedorRepository extends JpaRepository<Fornecedor, Long> {

	@Query(nativeQuery = true, value = "select * from fornecedor where id_unidade_cadastradora=?1")
	List<Fornecedor> findAll(Long id);

	@Query(nativeQuery=true, value="select * from fornecedor where cnpj=?1")
	Fornecedor findOne(String cnpj_contratada);
	
	@Query(nativeQuery=true, value="select * from fornecedor where cnpj is not null and nome is null")
	List<Fornecedor> getAllNotNull();
	
	@Query(nativeQuery=true, value="select f.nome, sum(c.valor_inicial) as valor from fornecedor f inner join contrato c on (c.cnpj_contratada=f.cnpj) inner join uasg u on (c.uasgid=u.id) inner join orgao o on (u.id_orgao=o.codigo) where o.nome=?1 group by f.nome order by sum(c.valor_inicial) desc")
	List<Object> getChartFornecedor(String orgao);
}
