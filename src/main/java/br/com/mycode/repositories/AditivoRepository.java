package br.com.mycode.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.mycode.entidades.Aditivo;

public interface AditivoRepository extends JpaRepository<Aditivo, Long> {

	@Query(nativeQuery = true, value = "select * from aditivo where contratof=?1")
	List<Aditivo> findAll(Long id);

}
