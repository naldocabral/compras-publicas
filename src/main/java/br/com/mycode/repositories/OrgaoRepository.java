package br.com.mycode.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.mycode.entidades.Orgao;
import br.com.mycode.views.ViewT;

public interface OrgaoRepository extends JpaRepository<Orgao, Long> {

	@Query(nativeQuery = true, value = "select u.nome as uasg, o.nome as orgao from uasg u, orgao o where u.id_orgao=o.id group by o.nome")
	List<ViewT> getOrgaoEUasg();
}
