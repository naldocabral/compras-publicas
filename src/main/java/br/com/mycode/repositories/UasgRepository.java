package br.com.mycode.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.mycode.entidades.Uasg;

public interface UasgRepository extends JpaRepository<Uasg, Long> {

	@Query(nativeQuery = true, value = "select * from uasg where id_orgao=?1")
	List<Uasg> findAll(Long orgaoId);

	@Query(nativeQuery=true, value="select * from uasg where id=?1")
	Uasg findOne(Long uasgid);

}
