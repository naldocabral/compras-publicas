package br.com.mycode.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.mycode.entidades.Cnae;

public interface CnaeRepository extends JpaRepository<Cnae, Long> {

}
