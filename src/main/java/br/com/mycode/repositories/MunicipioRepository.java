package br.com.mycode.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.mycode.entidades.Municipio;

public interface MunicipioRepository extends JpaRepository<Municipio, Long> {

}
