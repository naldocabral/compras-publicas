package br.com.mycode.repositories;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.mycode.util.ConfigCharts;
import br.com.mycode.util.Serie;


public class QueryJdbc {
	
	
	public ConfigCharts getQuery(String orgao, int dataInicial, int dataFinal){
		Connection con;
		PreparedStatement stm;
		String query = "select orgao, ano, total from view_orgao_ano_total where orgao='"+orgao+"' and ano between "+dataInicial+" and "+dataFinal+"";
		
		List<Serie> series = new ArrayList<Serie>();
		List<String> categorias = new ArrayList<String>();
		ConfigCharts charts = new ConfigCharts();
		
		try {
			con = new ConnectionFactory().getConnection();
			stm = con.prepareStatement(query);
			ResultSet rs = stm.executeQuery();
			
			while(rs.next()){
		
				String orga = rs.getString("orgao");
				
				if(!categorias.contains(orga)){
					categorias.add(orga);
				}
				
				Integer ano = rs.getInt("ano");
				BigDecimal total = rs.getBigDecimal("total");
				Serie serie = new Serie();
				serie.setName(ano.toString());
				serie.addDataValor(total);
				series.add(serie);
				System.out.println("Orgao: "+orga+" Ano: "+ano+" Total: "+total);
			}
			
			charts.setCategorias(categorias);
			charts.setSeries(series);
			stm.close();
			con.close();
			
				
					
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return charts;
	}
}
