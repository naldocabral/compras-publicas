package br.com.mycode.repositories;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.mycode.entidades.Contrato;
import br.com.mycode.views.OrgaoAnoTotalView;
import br.com.mycode.views.ValoresPorDataInicioVigencia;
import br.com.mycode.views.ViewContratoUasgPorOrgao;
import br.com.mycode.views.ViewSomaPorOrgao;

public interface ContratoRepository extends JpaRepository<Contrato, Long> {

	@Query(nativeQuery = true, value = "SELECT DISTINCT orgao FROM compraspublicas.view_orgao_ano_total")
	List<String> getOrgaos();
	
	//Seleciona agrupamento de soma dos valores iniciais e a data de inicio de vigencia dos contratos
	@Query(nativeQuery = true, value = "select * from soma_valor_por_data")
	List<ValoresPorDataInicioVigencia> getValorInicialContratos();

	@Query(nativeQuery = true, value = "select c.id as id_contrato, u.id as id_uasg, o.nome as nome_orgao from contrato c inner join  uasg u on c.uasg_id=u.id inner join orgao o on u.orgao_id=o.id")
	List<ViewContratoUasgPorOrgao> getTabelaContratoUasgOrgao();

	@Query(nativeQuery = true, value = "select sum(c.valor_inicial) as soma_valor_inicial, o.nome as orgao from contrato c inner join uasg u on (c.uasg_id=u.id) inner join orgao o on (u.orgao_id=o.id) group by o.nome")
	List<ViewSomaPorOrgao> getSomaValorInicialPorOrgao();

	@Query(nativeQuery = true, value = "select * from contrato where cnpj_contratada=?1")
	List<Contrato> findAllByCnpj(String cnpj);

	@Query(nativeQuery = true, value = "select * from contrato where uasgid=?1")
	List<Contrato> findAllByUasg(Long id);

	@Query(nativeQuery=true, value = "select * from contrato where licitacao_associada=?1")
	List<Contrato> findAllByLicitacao(String licitacao_associada);
	
	@Query(nativeQuery=true, value = "select o.nome as orgao, year(c.data_inicio_vigencia) as ano, sum(c.valor_inicial) as total from contrato c inner join uasg u on u.id=c.uasgid inner join orgao o on u.id_orgao=o.codigo group by o.nome, year(c.data_inicio_vigencia)")
	List<OrgaoAnoTotalView> getOrgaoAnoTotalList();

	/*@Query(nativeQuery=true, value = "")
	List<BigDecimal> getValores();
*/
	@Query(nativeQuery=true, value = "select o.nome as orgao, year(c.data_inicio_vigencia) as ano, sum(c.valor_inicial) as total from contrato c inner join uasg u on u.id=c.uasgid inner join orgao o on u.id_orgao=o.codigo group by o.nome, year(c.data_inicio_vigencia)")
	List<Object> getOrgaoAnoTotal();
	
	@Query(nativeQuery=true, value = "SELECT DISTINCT ano from view_orgao_ano_total where ano>2009")
	List<Integer> getAnos();
	
	
	@Query(nativeQuery=true, value = "SELECT DISTINCT total FROM view_orgao_ano_total where ano = ?1")
	List<BigDecimal> getValoresPorAno(Integer ano);
	
	
}
