package br.com.mycode.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.mycode.entidades.RamoDeNegocio;

public interface RamoDeNegocioRepository extends
		JpaRepository<RamoDeNegocio, Long> {

}
