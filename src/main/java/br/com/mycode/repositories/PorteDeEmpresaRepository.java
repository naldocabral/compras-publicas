package br.com.mycode.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.mycode.entidades.PorteDeEmpresa;

public interface PorteDeEmpresaRepository extends
		JpaRepository<PorteDeEmpresa, Long> {

}
