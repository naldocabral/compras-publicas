package br.com.mycode.repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
    public Connection getConnection() {
        try {
            return DriverManager.getConnection("jdbc:mysql://localhost/compraspublicas", "root", "opesp288");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}