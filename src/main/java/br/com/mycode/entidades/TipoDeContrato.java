package br.com.mycode.entidades;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class TipoDeContrato {

	public TipoDeContrato() {
		this.contratos = new ArrayList<Contrato>();
	}

	@JsonIgnore
	@Id
	private Long id;
	private String descricao;

	@Transient
	private String codigo;

	@OneToMany(mappedBy = "tipoDeContrato")
	private List<Contrato> contratos;

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
		this.setId(Long.parseLong(codigo));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Contrato> getContratos() {
		return contratos;
	}

	public void setContratos(List<Contrato> contratos) {
		this.contratos = contratos;
	}

	public void addContrato(Contrato contrato) {
		this.contratos.add(contrato);
	}

}
