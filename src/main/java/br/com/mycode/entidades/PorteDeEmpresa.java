package br.com.mycode.entidades;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class PorteDeEmpresa {

	@Id
	private Long id;

	private String descricao;

	@OneToMany(mappedBy = "porteDeEmpresa")
	private List<Fornecedor> fornecedores;

	public PorteDeEmpresa() {
		this.fornecedores = new ArrayList<Fornecedor>();
	}

	public void addFornecedor(Fornecedor fornecedor) {
		this.fornecedores.add(fornecedor);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Fornecedor> getFornecedores() {
		return fornecedores;
	}

	public void setFornecedores(List<Fornecedor> fornecedores) {
		this.fornecedores = fornecedores;
	}

}
