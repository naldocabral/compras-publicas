package br.com.mycode.entidades;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Municipio {

	@Id
	private Long id;

	private String nome;
	private String nome_uf;
	private String sigla_uf;
	private boolean ativo;

	@OneToMany(mappedBy = "municipio")
	private List<Fornecedor> fornecedores;

	@OneToMany(mappedBy = "municipio")
	private List<Uasg> uasgs;

	public Municipio() {
		this.fornecedores = new ArrayList<Fornecedor>();
		this.uasgs = new ArrayList<Uasg>();
	}

	public void addFornecedor(Fornecedor fornecedor) {
		this.fornecedores.add(fornecedor);
	}

	public void addUasg(Uasg uasg) {
		this.uasgs.add(uasg);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome_uf() {
		return nome_uf;
	}

	public void setNome_uf(String nome_uf) {
		this.nome_uf = nome_uf;
	}

	public String getSigla_uf() {
		return sigla_uf;
	}

	public void setSigla_uf(String sigla_uf) {
		this.sigla_uf = sigla_uf;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public List<Fornecedor> getFornecedores() {
		return fornecedores;
	}

	public void setFornecedores(List<Fornecedor> fornecedores) {
		this.fornecedores = fornecedores;
	}

	public List<Uasg> getUasgs() {
		return uasgs;
	}

	public void setUasgs(List<Uasg> uasgs) {
		this.uasgs = uasgs;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
