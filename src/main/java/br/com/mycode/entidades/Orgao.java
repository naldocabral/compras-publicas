package br.com.mycode.entidades;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Orgao {

	@Id
	private Long id;

	@OneToMany(mappedBy = "orgao", cascade = CascadeType.ALL)
	private List<Uasg> uasgs;

	private Long codigo;

	private String nome;
	private int codigo_tipo_adm;
	private int codigo_tipo_poder;
	private String codigo_siorg;
	private boolean ativo;

	public Orgao() {
		this.uasgs = new ArrayList<Uasg>();
	}

	public List<Uasg> getUasgs() {
		return uasgs;
	}

	public void setUasgs(List<Uasg> uasgs) {
		this.uasgs = uasgs;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
		this.setId(codigo);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getCodigo_tipo_adm() {
		return codigo_tipo_adm;
	}

	public void setCodigo_tipo_adm(int codigo_tipo_adm) {
		this.codigo_tipo_adm = codigo_tipo_adm;
	}

	public int getCodigo_tipo_poder() {
		return codigo_tipo_poder;
	}

	public void setCodigo_tipo_poder(int codigo_tipo_poder) {
		this.codigo_tipo_poder = codigo_tipo_poder;
	}

	public String getCodigo_siorg() {
		return codigo_siorg;
	}

	public void setCodigo_siorg(String codigo_siorg) {
		this.codigo_siorg = codigo_siorg;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public void addUasg(Uasg uasg) {
		this.uasgs.add(uasg);
	}

}
