package br.com.mycode.entidades;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class ModalidadeLicitacao {

	@Id
	@JsonProperty("codigo")
	private Long id;

	@OneToMany(mappedBy = "modalidade_licitacao")
	private List<Licitacao> licitacoes;

	@OneToMany(mappedBy = "modalidade_licitacao")
	private List<Contrato> contratos;

	private String descricao;

	public ModalidadeLicitacao() {
		this.contratos = new ArrayList<Contrato>();
		this.licitacoes = new ArrayList<Licitacao>();
	}

	public void addLicitacao(Licitacao licitacao) {
		this.licitacoes.add(licitacao);
	}

	public void addContrato(Contrato contrato) {
		this.contratos.add(contrato);
	}

	public Long getId() {
		return id;
	}

	public List<Licitacao> getLicitacoes() {
		return licitacoes;
	}

	public void setLicitacoes(List<Licitacao> licitacoes) {
		this.licitacoes = licitacoes;
	}

	public List<Contrato> getContratos() {
		return contratos;
	}

	public void setContratos(List<Contrato> contratos) {
		this.contratos = contratos;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
