package br.com.mycode.entidades;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Fornecedor {

	@Id
	private Long id;

	@OneToMany(mappedBy = "fornecedor")
	private List<Contrato> contratos;
	
	private String nome;
	private Long id_unidade_cadastradora;
	private String cnpj;
	private String razao_social;
	private String nome_fantasia;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@ManyToOne
	private Uasg uasg;
	@ManyToOne
	private NaturezaJuridica naturezaJuridica;

	private Long id_natureza_juridica;

	private Long id_porte_empresa;

	@ManyToOne
	private RamoDeNegocio ramoDeNegocio;
	@ManyToOne
	private PorteDeEmpresa porteDeEmpresa;
	@ManyToOne
	private Cnae cnae;

	private Long id_cnae;

	private Long id_municipio;

	private Long id_ramo_negocio;

	private String logradouro;
	private String numero_logradouro;
	private String bairro;

	@ManyToOne
	private Municipio municipio;

	private String cep;
	private boolean ativo;
	private boolean recadastrado;

	public Fornecedor() {
		this.contratos = new ArrayList<Contrato>();
	}

	public Long getId_unidade_cadastradora() {
		return id_unidade_cadastradora;
	}

	public void setId_unidade_cadastradora(Long id_unidade_cadastradora) {
		this.id_unidade_cadastradora = id_unidade_cadastradora;
	}

	public Long getId_ramo_negocio() {
		return id_ramo_negocio;
	}

	public void setId_ramo_negocio(Long id_ramo_negocio) {
		this.id_ramo_negocio = id_ramo_negocio;
	}

	public Long getId_municipio() {
		return id_municipio;
	}

	public void setId_municipio(Long id_municipio) {
		this.id_municipio = id_municipio;
	}

	public Long getId_cnae() {
		return id_cnae;
	}

	public void setId_cnae(Long id_cnae) {
		this.id_cnae = id_cnae;
	}

	public Long getId_porte_empresa() {
		return id_porte_empresa;
	}

	public void setId_porte_empresa(Long id_porte_empresa) {
		this.id_porte_empresa = id_porte_empresa;
	}

	public Long getId_natureza_juridica() {
		return id_natureza_juridica;
	}

	public void setId_natureza_juridica(Long id_natureza_juridica) {
		this.id_natureza_juridica = id_natureza_juridica;
	}

	public void addContrato(Contrato contrato) {
		this.contratos.add(contrato);
	}

	public List<Contrato> getContratos() {
		return contratos;
	}

	public void setContratos(List<Contrato> contratos) {
		this.contratos = contratos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazao_social() {
		return razao_social;
	}

	public void setRazao_social(String razao_social) {
		this.razao_social = razao_social;
	}

	public String getNome_fantasia() {
		return nome_fantasia;
	}

	public void setNome_fantasia(String nome_fantasia) {
		this.nome_fantasia = nome_fantasia;
	}

	public Uasg getUasg() {
		return uasg;
	}

	public void setUasg(Uasg uasg) {
		this.uasg = uasg;
	}

	public NaturezaJuridica getNaturezaJuridica() {
		return naturezaJuridica;
	}

	public void setNaturezaJuridica(NaturezaJuridica naturezaJuridica) {
		this.naturezaJuridica = naturezaJuridica;
	}

	public RamoDeNegocio getRamoDeNegocio() {
		return ramoDeNegocio;
	}

	public void setRamoDeNegocio(RamoDeNegocio ramoDeNegocio) {
		this.ramoDeNegocio = ramoDeNegocio;
	}

	public PorteDeEmpresa getPorteDeEmpresa() {
		return porteDeEmpresa;
	}

	public void setPorteDeEmpresa(PorteDeEmpresa porteDeEmpresa) {
		this.porteDeEmpresa = porteDeEmpresa;
	}

	public Cnae getCnae() {
		return cnae;
	}

	public void setCnae(Cnae cnae) {
		if (cnae.getId() == null) {

		} else {
			this.cnae = cnae;
		}
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero_logradouro() {
		return numero_logradouro;
	}

	public void setNumero_logradouro(String numero_logradouro) {
		this.numero_logradouro = numero_logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public boolean isRecadastrado() {
		return recadastrado;
	}

	public void setRecadastrado(boolean recadastrado) {
		this.recadastrado = recadastrado;
	}

}
