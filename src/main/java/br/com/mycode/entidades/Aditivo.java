package br.com.mycode.entidades;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Aditivo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@ManyToOne
	private Contrato contrato;

	@JsonIgnore
	@ManyToOne
	private Uasg uasg;

	@JsonProperty("contrato")
	private String contratoF;

	//Chave identificadora: concatena codigo+numero e transforma em um Long
	private String codigo;
	private String numero;

	private int modalidade_termo;

	private String numero_termo;

	@Column(columnDefinition = "text")
	private String objeto_aditivo;

	private String fundamento_legal_aditivo;

	@Column(columnDefinition = "date")
	private Date data_assinatura_aditivo;

	public String getContratoF() {
		return contratoF;
	}

	public void setContratoF(String contratoF) {
		this.contratoF = contratoF;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Uasg getUasg() {
		return uasg;
	}

	public void setUasg(Uasg uasg) {
		this.uasg = uasg;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public int getModalidade_termo() {
		return modalidade_termo;
	}

	public void setModalidade_termo(int modalidade_termo) {
		this.modalidade_termo = modalidade_termo;
	}

	public String getNumero_termo() {
		return numero_termo;
	}

	public void setNumero_termo(String numero_termo) {
		this.numero_termo = numero_termo;
	}

	public String getObjeto_aditivo() {
		return objeto_aditivo;
	}

	public void setObjeto_aditivo(String objeto_aditivo) {
		this.objeto_aditivo = objeto_aditivo;
	}

	public String getFundamento_legal_aditivo() {
		return fundamento_legal_aditivo;
	}

	public void setFundamento_legal_aditivo(String fundamento_legal_aditivo) {
		this.fundamento_legal_aditivo = fundamento_legal_aditivo;
	}

	public Date getData_assinatura_aditivo() {
		return data_assinatura_aditivo;
	}

	public void setData_assinatura_aditivo(Date data_assinatura_aditivo) {
		this.data_assinatura_aditivo = data_assinatura_aditivo;
	}

}
