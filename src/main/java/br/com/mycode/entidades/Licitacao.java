package br.com.mycode.entidades;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Licitacao {

	@Id
	private Long id;

	private String identificador;

	@JsonProperty("uasg")
	private String uasgFalsa;

	@JsonIgnore
	@ManyToOne
	private Uasg uasg;

	@OneToMany(mappedBy = "licitacao")
	private List<Contrato> contratos;

	private String modalidade;

	@ManyToOne
	private ModalidadeLicitacao modalidade_licitacao;

	private String numero_aviso;
	private String situacao_aviso;

	@Column(columnDefinition = "text")
	private String objeto;

	private String informacoes_gerais;
	private String numero_processo;
	private String tipo_recurso;
	private String numero_itens;
	private String nome_responsavel;
	private String funcao_responsavel;
	private String endereco_entrega_edital;

	public Licitacao() {
		this.contratos = new ArrayList<Contrato>();
	}

	public Uasg getUasg() {
		return uasg;
	}

	public void setUasg(Uasg uasg) {
		this.uasg = uasg;
	}

	public List<Contrato> getContratos() {
		return contratos;
	}

	public void setContratos(List<Contrato> contratos) {
		this.contratos = contratos;
	}

	public ModalidadeLicitacao getModalidade_licitacao() {
		return modalidade_licitacao;
	}

	public void setModalidade_licitacao(ModalidadeLicitacao modalidade_licitacao) {
		this.modalidade_licitacao = modalidade_licitacao;
	}

	public void addContrato(Contrato contrato) {
		this.contratos.add(contrato);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUasgFalsa() {
		return this.uasgFalsa;
	}

	public void setUasgFalsa(String uasgFalsa) {
		this.uasgFalsa = uasgFalsa;
	}

	public String getModalidade() {
		return modalidade;
	}

	public void setModalidade(String modalidade) {
		this.modalidade = modalidade;
	}

	public String getNumero_aviso() {
		return numero_aviso;
	}

	public void setNumero_aviso(String numero_aviso) {
		this.numero_aviso = numero_aviso;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
		this.setId(Long.parseLong(identificador));
	}

	public String getSituacao_aviso() {
		return situacao_aviso;
	}

	public void setSituacao_aviso(String situacao_aviso) {
		this.situacao_aviso = situacao_aviso;
	}

	public String getObjeto() {
		return objeto;
	}

	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}

	public String getInformacoes_gerais() {
		return informacoes_gerais;
	}

	public void setInformacoes_gerais(String informacoes_gerais) {
		this.informacoes_gerais = informacoes_gerais;
	}

	public String getNumero_processo() {
		return numero_processo;
	}

	public void setNumero_processo(String numero_processo) {
		this.numero_processo = numero_processo;
	}

	public String getTipo_recurso() {
		return tipo_recurso;
	}

	public void setTipo_recurso(String tipo_recurso) {
		this.tipo_recurso = tipo_recurso;
	}

	public String getNumero_itens() {
		return numero_itens;
	}

	public void setNumero_itens(String numero_itens) {
		this.numero_itens = numero_itens;
	}

	public String getNome_responsavel() {
		return nome_responsavel;
	}

	public void setNome_responsavel(String nome_responsavel) {
		this.nome_responsavel = nome_responsavel;
	}

	public String getFuncao_responsavel() {
		return funcao_responsavel;
	}

	public void setFuncao_responsavel(String funcao_responsavel) {
		this.funcao_responsavel = funcao_responsavel;
	}

	public String getEndereco_entrega_edital() {
		return endereco_entrega_edital;
	}

	public void setEndereco_entrega_edital(String endereco_entrega_edital) {
		this.endereco_entrega_edital = endereco_entrega_edital;
	}

}
