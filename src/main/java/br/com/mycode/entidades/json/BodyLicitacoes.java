package br.com.mycode.entidades.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BodyLicitacoes {

	@JsonProperty("embedded")
	private EmbeddedLicitacoes embedded;
	private int count;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public EmbeddedLicitacoes getEmbedded() {
		return embedded;
	}

	public void setEmbedded(EmbeddedLicitacoes embedded) {
		this.embedded = embedded;
	}

}
