package br.com.mycode.entidades.json;

import java.util.ArrayList;
import java.util.List;

import br.com.mycode.entidades.PorteDeEmpresa;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmbeddedPorteDeEmpresa {

	@JsonProperty("portesEmpresa")
	private List<PorteDeEmpresa> portesDeEmpresa;

	public EmbeddedPorteDeEmpresa() {
		this.portesDeEmpresa = new ArrayList<PorteDeEmpresa>();
	}

	public List<PorteDeEmpresa> getPortesDeEmpresa() {
		return portesDeEmpresa;
	}

	public void setPortesDeEmpresa(List<PorteDeEmpresa> portesDeEmpresa) {
		this.portesDeEmpresa = portesDeEmpresa;
	}

}
