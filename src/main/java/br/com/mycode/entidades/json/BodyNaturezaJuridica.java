package br.com.mycode.entidades.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BodyNaturezaJuridica {

	@JsonProperty("_embedded")
	private EmbeddedNaturezaJuridica embedded;

	private int count;

	public EmbeddedNaturezaJuridica getEmbedded() {
		return embedded;
	}

	public void setEmbedded(EmbeddedNaturezaJuridica embedded) {
		this.embedded = embedded;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
