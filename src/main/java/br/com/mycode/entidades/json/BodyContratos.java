package br.com.mycode.entidades.json;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class BodyContratos {

	@JsonProperty("_embedded")
	private EmbeddedContratos embedded;
	private int count;

	public void setCount(int count) {
		this.count = count;
	}

	public EmbeddedContratos getEmbedded() {
		return this.embedded;
	}

	public void setEmbedded(EmbeddedContratos embedded) {
		this.embedded = embedded;

	}

	public int getCount() {
		// TODO Auto-generated method stub
		return this.count;
	}

}
