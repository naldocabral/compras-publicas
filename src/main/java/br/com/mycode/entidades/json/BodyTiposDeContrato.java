package br.com.mycode.entidades.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BodyTiposDeContrato {

	@JsonProperty("_embedded")
	private EmbeddedTiposDeContrato embedded;
	private int count;

	public EmbeddedTiposDeContrato getEmbedded() {
		return embedded;
	}

	public void setEmbedded(EmbeddedTiposDeContrato embedded) {
		this.embedded = embedded;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
