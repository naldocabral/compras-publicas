package br.com.mycode.entidades.json;

import java.util.ArrayList;
import java.util.List;

import br.com.mycode.entidades.Orgao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmbeddedOrgaos {

	@JsonProperty("Orgaos")
	private List<Orgao> orgaos;

	public EmbeddedOrgaos() {
		this.orgaos = new ArrayList<Orgao>();
	}

	public List<Orgao> getOrgaos() {
		return orgaos;
	}

	public void setOrgaos(List<Orgao> orgaos) {
		this.orgaos = orgaos;
	}

}
