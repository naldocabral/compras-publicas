package br.com.mycode.entidades.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BodyOrgaos {

	@JsonProperty("_embedded")
	private EmbeddedOrgaos embedded;

	private int count;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public EmbeddedOrgaos getEmbedded() {
		return embedded;
	}

	public void setEmbedded(EmbeddedOrgaos embedded) {
		this.embedded = embedded;
	}

}
