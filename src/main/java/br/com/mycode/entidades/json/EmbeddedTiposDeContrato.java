package br.com.mycode.entidades.json;

import java.util.ArrayList;
import java.util.List;

import br.com.mycode.entidades.TipoDeContrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmbeddedTiposDeContrato {

	@JsonProperty("TiposContrato")
	private List<TipoDeContrato> tiposDeContrato;

	public EmbeddedTiposDeContrato() {
		this.tiposDeContrato = new ArrayList<TipoDeContrato>();
	}

	public List<TipoDeContrato> getTiposDeContrato() {
		return tiposDeContrato;
	}

	public void setTiposDeContrato(List<TipoDeContrato> tiposDeContrato) {
		this.tiposDeContrato = tiposDeContrato;
	}

}
