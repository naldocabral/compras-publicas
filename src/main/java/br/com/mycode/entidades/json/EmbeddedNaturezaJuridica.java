package br.com.mycode.entidades.json;

import java.util.ArrayList;
import java.util.List;

import br.com.mycode.entidades.NaturezaJuridica;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmbeddedNaturezaJuridica {

	@JsonProperty("naturezasJuridicas")
	private List<NaturezaJuridica> naturezasJuridicas;

	public EmbeddedNaturezaJuridica() {
		this.naturezasJuridicas = new ArrayList<NaturezaJuridica>();
	}

	public List<NaturezaJuridica> getNaturezasJuridica() {
		return naturezasJuridicas;
	}

	public void setNaturezasJuridica(List<NaturezaJuridica> naturezasJuridica) {
		this.naturezasJuridicas = naturezasJuridica;
	}

}
