package br.com.mycode.entidades.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BodyModalidadeLicitacao {

	@JsonProperty("_embedded")
	private EmbeddedModalidadeLicitacao embedded;

	private int count;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public EmbeddedModalidadeLicitacao getEmbedded() {
		return embedded;
	}

	public void setEmbedded(EmbeddedModalidadeLicitacao embedded) {
		this.embedded = embedded;
	}

}
