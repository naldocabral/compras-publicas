package br.com.mycode.entidades.json;

import java.util.ArrayList;
import java.util.List;

import br.com.mycode.entidades.Aditivo;
import br.com.mycode.entidades.Cnae;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmbeddedCnaes {

	@JsonProperty("cnaes")
	private List<Cnae> cnaes;

	public EmbeddedCnaes() {
		this.cnaes = new ArrayList<Cnae>();
	}

	public List<Cnae> getCnaes() {
		return cnaes;
	}

	public void setCnaes(List<Cnae> cnaes) {
		this.cnaes = cnaes;
	}

}
