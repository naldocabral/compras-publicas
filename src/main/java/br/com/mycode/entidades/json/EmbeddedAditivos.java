package br.com.mycode.entidades.json;

import java.util.List;

import br.com.mycode.entidades.Aditivo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmbeddedAditivos {

	private List<Aditivo> aditivos;

	public List<Aditivo> getAditivos() {
		return aditivos;
	}

	public void setAditivos(List<Aditivo> aditivos) {
		this.aditivos = aditivos;
	}

}
