package br.com.mycode.entidades.json;

import java.util.ArrayList;
import java.util.List;

import br.com.mycode.entidades.ModalidadeLicitacao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmbeddedModalidadeLicitacao {

	@JsonProperty("ModalidadesLicitacao")
	private List<ModalidadeLicitacao> modalidadesLicitacao;

	public EmbeddedModalidadeLicitacao() {
		this.modalidadesLicitacao = new ArrayList<ModalidadeLicitacao>();
	}

	public List<ModalidadeLicitacao> getModalidadesLicitacao() {
		return modalidadesLicitacao;
	}

	public void setModalidadesLicitacao(
			List<ModalidadeLicitacao> modalidadesLicitacao) {
		this.modalidadesLicitacao = modalidadesLicitacao;
	}

}
