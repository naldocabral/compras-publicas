package br.com.mycode.entidades.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BodyFornecedores {

	@JsonProperty("_embedded")
	private EmbeddedFornecedores embedded;

	private int count;

	public EmbeddedFornecedores getEmbedded() {
		return embedded;
	}

	public void setEmbedded(EmbeddedFornecedores embedded) {
		this.embedded = embedded;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
