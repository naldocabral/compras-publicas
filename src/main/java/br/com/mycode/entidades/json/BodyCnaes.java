package br.com.mycode.entidades.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BodyCnaes {

	@JsonProperty("_embedded")
	private EmbeddedCnaes embedded;

	private int count;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public EmbeddedCnaes getEmbedded() {
		return embedded;
	}

	public void setEmbedded(EmbeddedCnaes embedded) {
		this.embedded = embedded;
	}

}
