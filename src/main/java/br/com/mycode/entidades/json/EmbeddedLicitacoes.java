package br.com.mycode.entidades.json;

import java.util.ArrayList;
import java.util.List;

import br.com.mycode.entidades.Licitacao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmbeddedLicitacoes {

	@JsonProperty("licitacoes")
	private List<Licitacao> licitacoes;

	public EmbeddedLicitacoes() {
		this.licitacoes = new ArrayList<Licitacao>();
	}

	public List<Licitacao> getLicitacoes() {
		return licitacoes;
	}

	public void setLicitacoes(List<Licitacao> licitacoes) {
		this.licitacoes = licitacoes;
	}

}
