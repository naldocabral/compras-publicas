package br.com.mycode.entidades.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BodyPorteDeEmpresa {

	@JsonProperty("_embedded")
	private EmbeddedPorteDeEmpresa embedded;

	private int count;

	public EmbeddedPorteDeEmpresa getEmbedded() {
		return embedded;
	}

	public void setEmbedded(EmbeddedPorteDeEmpresa embedded) {
		this.embedded = embedded;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
