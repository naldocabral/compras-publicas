package br.com.mycode.entidades.json;

import java.util.List;

import org.springframework.stereotype.Component;

import br.com.mycode.entidades.Contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Component("embeddedContratoImpl")
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmbeddedContratos {

	@JsonProperty("contratos")
	private List<Contrato> contratos;

	public List<Contrato> getContratos() {
		return contratos;
	}

	public void setContratos(List<Contrato> contratos) {
		this.contratos = contratos;
	}

}
