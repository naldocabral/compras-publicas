package br.com.mycode.entidades.json;

import java.util.ArrayList;
import java.util.List;

import br.com.mycode.entidades.Fornecedor;
import br.com.mycode.entidades.Municipio;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmbeddedFornecedores {

	@JsonProperty("fornecedores")
	private List<Fornecedor> fornecedores;

	public EmbeddedFornecedores() {
		this.fornecedores = new ArrayList<Fornecedor>();
	}

	public List<Fornecedor> getFornecedores() {
		return fornecedores;
	}

	public void setFornecedores(List<Fornecedor> fornecedores) {
		this.fornecedores = fornecedores;
	}

}
