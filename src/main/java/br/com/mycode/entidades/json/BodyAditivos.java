package br.com.mycode.entidades.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BodyAditivos {

	@JsonProperty("_embedded")
	private EmbeddedAditivos embedded;

	private int count;

	public void setCount(int count) {
		this.count = count;
	}

	public EmbeddedAditivos getEmbedded() {
		return embedded;
	}

	public void setEmbedded(EmbeddedAditivos embedded) {
		this.embedded = embedded;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return this.count;
	}

}
