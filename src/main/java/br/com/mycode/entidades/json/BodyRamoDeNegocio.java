package br.com.mycode.entidades.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BodyRamoDeNegocio {

	@JsonProperty("_embedded")
	private EmbeddedRamoDeNegocio embedded;

	private int count;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public EmbeddedRamoDeNegocio getEmbedded() {
		return embedded;
	}

	public void setEmbedded(EmbeddedRamoDeNegocio embedded) {
		this.embedded = embedded;
	}

}
