package br.com.mycode.entidades.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BodyUasgs {

	@JsonProperty("_embedded")
	private EmbeddedUasgs embedded;

	private int count;

	public EmbeddedUasgs getEmbedded() {
		return embedded;
	}

	public void setEmbedded(EmbeddedUasgs embedded) {
		this.embedded = embedded;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
