package br.com.mycode.entidades.json;

import java.util.ArrayList;
import java.util.List;

import br.com.mycode.entidades.RamoDeNegocio;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmbeddedRamoDeNegocio {

	@JsonProperty("ramosNegocio")
	private List<RamoDeNegocio> ramosDeNegocio;

	public EmbeddedRamoDeNegocio() {
		this.ramosDeNegocio = new ArrayList<RamoDeNegocio>();
	}

	public List<RamoDeNegocio> getRamosDeNegocio() {
		return ramosDeNegocio;
	}

	public void setRamosDeNegocio(List<RamoDeNegocio> ramosDeNegocio) {
		this.ramosDeNegocio = ramosDeNegocio;
	}

}
