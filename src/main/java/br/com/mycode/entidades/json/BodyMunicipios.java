package br.com.mycode.entidades.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BodyMunicipios {

	@JsonProperty("_embedded")
	private EmbeddedMunicipios embedded;
	private int count;

	public EmbeddedMunicipios getEmbedded() {
		return embedded;
	}

	public void setEmbedded(EmbeddedMunicipios embedded) {
		this.embedded = embedded;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
