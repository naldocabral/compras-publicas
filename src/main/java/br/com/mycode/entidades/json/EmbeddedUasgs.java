package br.com.mycode.entidades.json;

import java.util.List;

import br.com.mycode.entidades.Uasg;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmbeddedUasgs {

	private List<Uasg> uasgs;

	public List<Uasg> getUasgs() {
		return uasgs;
	}

	public void setUasgs(List<Uasg> uasgs) {
		this.uasgs = uasgs;
	}

}
