package br.com.mycode.entidades;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class RamoDeNegocio {

	@Id
	private Long id;

	@OneToMany(mappedBy = "ramoDeNegocio")
	private List<Fornecedor> fornecedores;

	private String descricao;
	private boolean ativo;

	public RamoDeNegocio() {
		this.fornecedores = new ArrayList<Fornecedor>();
	}

	public Long getId() {
		return id;
	}

	public void addFornecedor(Fornecedor fornecedor) {
		this.fornecedores.add(fornecedor);
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Fornecedor> getFornecedores() {
		return fornecedores;
	}

	public void setFornecedores(List<Fornecedor> fornecedores) {
		this.fornecedores = fornecedores;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

}
