package br.com.mycode.entidades;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Contrato {

	public Contrato() {
		this.aditivos = new ArrayList<Aditivo>();
	}

	@Id
	private Long id;

	private String identificador;

	@JsonProperty("uasg")
	private String uasgid;

	@JsonIgnore
	@ManyToOne
	private Uasg uasg;

	@ManyToOne
	private TipoDeContrato tipoDeContrato;

	@OneToMany(mappedBy = "contrato")
	private List<Aditivo> aditivos;

	private String tipo_contrato;

	@JsonProperty("modalidade_licitacao")
	private String modalidade_licit;

	@JsonIgnore
	@ManyToOne
	private ModalidadeLicitacao modalidade_licitacao;

	private String numero_licitacao;

	private String licitacao_associada;

	@ManyToOne
	private Licitacao licitacao;

	private String origem_licitacao;
	private String numero;

	@Column(columnDefinition = "text")
	private String objeto;

	private int numero_aditivo;
	private String numero_processo;

	private String cnpj_contratada;

	@OneToOne
	private Fornecedor fornecedor;

	private Date data_assinatura;
	private String fundamento_legal;
	private Date data_inicio_vigencia;
	private Date data_termino_vigencia;
	private double valor_inicial;

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		if (fornecedor.getId() == null) {
			System.out.println("Fornecedor nulo ou pessoa física");
		} else {
			this.fornecedor = fornecedor;
		}

	}

	public String getModalidade_licit() {
		return modalidade_licit;
	}

	public void setModalidade_licit(String modalidade_licit) {
		this.modalidade_licit = modalidade_licit;
	}

	public ModalidadeLicitacao getModalidade_licitacao() {
		return modalidade_licitacao;
	}

	public void setModalidade_licitacao(ModalidadeLicitacao modalidade_licitacao) {
		this.modalidade_licitacao = modalidade_licitacao;
	}

	public Licitacao getLicitacao() {
		return licitacao;
	}

	public void setLicitacao(Licitacao licitacao) {
		if (licitacao.getId() != null) {
			this.licitacao = licitacao;
		} else {

		}

	}

	public void addAditivo(Aditivo aditivo) {
		this.aditivos.add(aditivo);
	}

	public List<Aditivo> getAditivos() {
		return aditivos;
	}

	public void setAditivos(List<Aditivo> aditivos) {
		this.aditivos = aditivos;
	}

	public String getTipo_contrato() {
		return tipo_contrato;
	}

	public void setTipo_contrato(String tipo_contrato) {
		this.tipo_contrato = tipo_contrato;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdentificador() {
		return identificador;
	}

	//deixando a chave burra menos burra
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
		this.setId(Long.parseLong(identificador));
	}

	public TipoDeContrato getTipoDeContrato() {
		return tipoDeContrato;
	}

	public void setTipoDeContrato(TipoDeContrato tipoDeContrato) {
		this.tipoDeContrato = tipoDeContrato;
	}

	public String getUasgid() {
		return uasgid;
	}

	public void setUasgid(String uasgid) {
		this.uasgid = uasgid;
	}

	public Uasg getUasg() {
		return uasg;
	}

	public void setUasg(Uasg uasg) {
		this.uasg = uasg;
	}

	public String getNumero_licitacao() {
		return numero_licitacao;
	}

	public void setNumero_licitacao(String numero_licitacao) {
		this.numero_licitacao = numero_licitacao;
	}

	public String getLicitacao_associada() {
		return licitacao_associada;
	}

	public void setLicitacao_associada(String licitacao_associada) {
		this.licitacao_associada = licitacao_associada;
	}

	public String getOrigem_licitacao() {
		return origem_licitacao;
	}

	public void setOrigem_licitacao(String origem_licitacao) {
		this.origem_licitacao = origem_licitacao;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getObjeto() {
		return objeto;
	}

	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}

	public int getNumero_aditivo() {
		return numero_aditivo;
	}

	public void setNumero_aditivo(int numero_aditivo) {
		this.numero_aditivo = numero_aditivo;
	}

	public String getNumero_processo() {
		return numero_processo;
	}

	public void setNumero_processo(String numero_processo) {
		this.numero_processo = numero_processo;
	}

	public String getCnpj_contratada() {
		return cnpj_contratada;
	}

	public void setCnpj_contratada(String cnpj_contratada) {
		this.cnpj_contratada = cnpj_contratada;
	}

	public Date getData_assinatura() {
		return data_assinatura;
	}

	public void setData_assinatura(Date data_assinatura) {
		this.data_assinatura = data_assinatura;
	}

	public String getFundamento_legal() {
		return fundamento_legal;
	}

	public void setFundamento_legal(String fundamento_legal) {
		this.fundamento_legal = fundamento_legal;
	}

	public Date getData_inicio_vigencia() {
		return data_inicio_vigencia;
	}

	public void setData_inicio_vigencia(Date data_inicio_vigencia) {
		this.data_inicio_vigencia = data_inicio_vigencia;
	}

	public Date getData_termino_vigencia() {
		return data_termino_vigencia;
	}

	public void setData_termino_vigencia(Date data_termino_vigencia) {
		this.data_termino_vigencia = data_termino_vigencia;
	}

	public double getValor_inicial() {
		return valor_inicial;
	}

	public void setValor_inicial(double valor_inicial) {
		this.valor_inicial = valor_inicial;
	}

	@Override
	public String toString() {
		return "Contrato [id=" + id + ", uasg=" + uasg
				+ ", modalidade_licitacao=" + modalidade_licitacao
				+ ", valor_inicial=" + valor_inicial + "]";
	}

}
