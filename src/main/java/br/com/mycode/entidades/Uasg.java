package br.com.mycode.entidades;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Uasg {

	public Uasg() {
		this.constratos = new ArrayList<Contrato>();
		this.fornecedores = new ArrayList<Fornecedor>();
		this.licitacoes = new ArrayList<Licitacao>();
	}

	@Id
	private Long id;

	@OneToMany(mappedBy="uasg", cascade=CascadeType.ALL)
	private List<Licitacao> licitacoes;
	
	@OneToMany(mappedBy = "uasg", cascade = CascadeType.ALL)
	private List<Fornecedor> fornecedores;

	@OneToMany(mappedBy = "uasg", cascade = CascadeType.ALL)
	List<Contrato> constratos;

	private String nome;

	@JsonProperty("id_orgao")
	private Long idOrgao;

	@ManyToOne
	private Municipio municipio;

	@ManyToOne(cascade = CascadeType.ALL)
	private Orgao orgao;

	private Long id_municipio;
	private String cep;
	private String total_fornecedores_cadastrados;
	private int total_fornecedores_recadastrados;
	private boolean unidade_cadastradora;
	private boolean ativo;

	
	
	public List<Licitacao> getLicitacoes() {
		return licitacoes;
	}

	public void setLicitacoes(List<Licitacao> licitacoes) {
		this.licitacoes = licitacoes;
	}

	public List<Fornecedor> getFornecedores() {
		return fornecedores;
	}

	public void setFornecedores(List<Fornecedor> fornecedores) {
		this.fornecedores = fornecedores;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public Long getIdOrgao() {
		return idOrgao;
	}

	public void setIdOrgao(Long idOrgao) {
		this.idOrgao = idOrgao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Contrato> getConstratos() {
		return constratos;
	}

	public void setConstratos(List<Contrato> constratos) {
		this.constratos = constratos;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Orgao getOrgao() {
		return orgao;
	}

	public void setOrgao(Orgao orgao) {
		this.orgao = orgao;
	}

	public Long getId_municipio() {
		return id_municipio;
	}

	public void setId_municipio(Long id_municipio) {
		this.id_municipio = id_municipio;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getTotal_fornecedores_cadastrados() {
		return total_fornecedores_cadastrados;
	}

	public void setTotal_fornecedores_cadastrados(
			String total_fornecedores_cadastrados) {
		this.total_fornecedores_cadastrados = total_fornecedores_cadastrados;
	}

	public int getTotal_fornecedores_recadastrados() {
		return total_fornecedores_recadastrados;
	}

	public void setTotal_fornecedores_recadastrados(
			int total_fornecedores_recadastrados) {
		this.total_fornecedores_recadastrados = total_fornecedores_recadastrados;
	}

	public boolean isUnidade_cadastradora() {
		return unidade_cadastradora;
	}

	public void setUnidade_cadastradora(boolean unidade_cadastradora) {
		this.unidade_cadastradora = unidade_cadastradora;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public void addContrato(Contrato contrato) {
		this.constratos.add(contrato);

	}

}
