package br.com.mycode.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Serie {
	private String name;
	List<BigDecimal> data;

	public Serie() {
		this.data = new ArrayList<BigDecimal>();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<BigDecimal> getData() {
		return data;
	}
	public void setData(List<BigDecimal> data) {
		this.data = data;
	}
	public void addDataValor(BigDecimal valor){
		this.data.add(valor);
	}
	
	
}
