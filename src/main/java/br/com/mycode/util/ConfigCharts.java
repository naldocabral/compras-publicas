package br.com.mycode.util;

import java.util.ArrayList;
import java.util.List;

public class ConfigCharts {
	private List<String> categorias;
	private List<Serie> series;
	
	public ConfigCharts (){
		this.categorias = new ArrayList<String>();
		this.series = new ArrayList<Serie>();
	}
	
	public List<String> getCategorias() {
		return categorias;
	}
	public void setCategorias(List<String> categorias) {
		this.categorias = categorias;
	}
	public List<Serie> getSeries() {
		return series;
	}
	public void setSeries(List<Serie> series) {
		this.series = series;
	}
	
	
}
