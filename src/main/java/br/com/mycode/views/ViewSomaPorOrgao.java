package br.com.mycode.views;

import javax.persistence.Table;

@Table(name = "soma_por_orgao")
public class ViewSomaPorOrgao {

	private double soma_valor_inicial;
	private String orgao;

	public double getSoma_valor_inicial() {
		return soma_valor_inicial;
	}

	public void setSoma_valor_inicial(double soma_valor_inicial) {
		this.soma_valor_inicial = soma_valor_inicial;
	}

	public String getOrgao() {
		return orgao;
	}

	public void setOrgao(String orgao) {
		this.orgao = orgao;
	}

}
