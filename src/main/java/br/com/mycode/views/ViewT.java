package br.com.mycode.views;

import javax.persistence.Table;

//@Table(name="view_teste")
public class ViewT {

	private String uasg;
	private String orgao;

	public String getUasg() {
		return uasg;
	}

	public void setUasg(String uasg) {
		this.uasg = uasg;
	}

	public String getOrgao() {
		return orgao;
	}

	public void setOrgao(String orgao) {
		this.orgao = orgao;
	}

}
