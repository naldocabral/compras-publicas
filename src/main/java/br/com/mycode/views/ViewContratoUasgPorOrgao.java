package br.com.mycode.views;

import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "contrato_uasg_orgao")
public class ViewContratoUasgPorOrgao {

	private Long id_contrato;
	private Long id_uasg;
	private String nome_orgao;

	public Long getId_contrato() {
		return id_contrato;
	}

	public void setId_contrato(Long id_contrato) {
		this.id_contrato = id_contrato;
	}

	public Long getId_uasg() {
		return id_uasg;
	}

	public void setId_uasg(Long id_uasg) {
		this.id_uasg = id_uasg;
	}

	public String getNome_orgao() {
		return nome_orgao;
	}

	public void setNome_orgao(String nome_orgao) {
		this.nome_orgao = nome_orgao;
	}

}
