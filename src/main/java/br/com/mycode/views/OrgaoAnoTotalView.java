package br.com.mycode.views;

import java.math.BigDecimal;

public class OrgaoAnoTotalView {
	
	private String orgao;
	private String ano;
	private BigDecimal total;
	
	public String getOrgao() {
		return orgao;
	}
	public void setOrgao(String orgao) {
		this.orgao = orgao;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	
	

}
