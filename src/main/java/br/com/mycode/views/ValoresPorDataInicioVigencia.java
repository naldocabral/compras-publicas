package br.com.mycode.views;

import java.sql.Date;

import javax.persistence.Table;

public class ValoresPorDataInicioVigencia {
	
	private Date data_inicio_vigencia;
	private Double soma_valor_inicial;
	
	public Date getData_inicio_vigencia() {
		return data_inicio_vigencia;
	}
	public void setData_inicio_vigencia(Date data_inicio_vigencia) {
		this.data_inicio_vigencia = data_inicio_vigencia;
	}
	public double getSoma_valor_inicial() {
		return soma_valor_inicial;
	}
	public void setSoma_valor_inicial(double soma_valor_inicial) {
		this.soma_valor_inicial = soma_valor_inicial;
	}
	
	
	
	
}
