package br.com.mycode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import br.com.mycode.entidades.Contrato;

@SpringBootApplication
@Controller
public class ComprasPublicasApplication {

	@RequestMapping("/")
	public String getIndex() {
		return "pages/index.html";
	}

	public static void main(String[] args) {
		SpringApplication.run(ComprasPublicasApplication.class, args);

	}
}
