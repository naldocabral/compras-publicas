package br.com.mycode.controles;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.mycode.entidades.Orgao;
import br.com.mycode.repositories.ContratoRepository;
import br.com.mycode.repositories.FornecedorRepository;
import br.com.mycode.repositories.OrgaoRepository;
import br.com.mycode.repositories.QueryJdbc;
import br.com.mycode.util.ConfigCharts;
import br.com.mycode.util.Serie;
import br.com.mycode.views.ValoresPorDataInicioVigencia;
import br.com.mycode.views.ViewContratoUasgPorOrgao;
import br.com.mycode.views.ViewSomaPorOrgao;
import br.com.mycode.views.ViewT;

@RestController
public class QueryController {

	private static final Logger log = LoggerFactory
			.getLogger(QueryController.class);

	@Autowired
	private OrgaoRepository orgaoRepository;

	@Autowired
	private ContratoRepository contratoRepository;
	
	@Autowired
	private FornecedorRepository fornecedorRepository;

	//@RequestParam(value="id") Long id
	
	@RequestMapping(value = "/filtroFornecedor", method = RequestMethod.GET)
	public List<Object> getChartFornecedor(@RequestParam(value="orgao") String orgao){
		return this.fornecedorRepository.getChartFornecedor(orgao);
	}

	@RequestMapping(value = "/filtro", method = RequestMethod.GET)
	public ConfigCharts testandoQuery(@RequestParam String orgao, Integer dataInicial, Integer dataFinal) {
		QueryJdbc jdbc = new QueryJdbc();
		return jdbc.getQuery(orgao, dataInicial, dataFinal);
	}
	
	@RequestMapping(value = "/buscaOrgaos", method = RequestMethod.GET)
	public List<String> buscaOrgaos() {
		List<String> orgaos = new ArrayList<String>();
		for(Orgao o : this.orgaoRepository.findAll()){
			if(!orgaos.contains(o.getNome())){
				orgaos.add(o.getNome());
			}
			
		}
		return orgaos;
		
		/*List<Orgao> orgaos = new ArrayList<Orgao>();
		orgaos = this.orgaoRepository.findAll();
		return orgaos;*/
	}
	
	@RequestMapping(value = "/query/soma/contrato", method = RequestMethod.GET)
	public List<ValoresPorDataInicioVigencia> getSomaValorInicialDeContratos() {
		//log.info("SOMA DE CONTRATOS DO BD: "+this.contratoRepository.SomaPorOrgao());
		return this.contratoRepository.getValorInicialContratos();
	}
	
	@RequestMapping(value = "/query/agrupar/orgao/ano/total", method = RequestMethod.GET)
	public ConfigCharts getConfigChartsBasicBar() {
		//log.info("SOMA DE CONTRATOS DO BD: "+this.contratoRepository.SomaPorOrgao());
		List<Integer> anos = new ArrayList<Integer>();
		List<String> categorias = new ArrayList<String>();
		categorias = this.contratoRepository.getOrgaos();
		anos = this.contratoRepository.getAnos();

		//configuração charts
		List<Serie> series = new ArrayList<Serie>();
		
		for(Integer ano: anos){
			Serie serie = new Serie();
			serie.setName(ano.toString());
			serie.setData(this.contratoRepository.getValoresPorAno(ano));
			series.add(serie);
		}
		
		ConfigCharts config = new ConfigCharts();
		config.setSeries(series);
		config.setCategorias(categorias);
		
		return config;
	}

	@RequestMapping(value = "/query/tabelacontrato", method = RequestMethod.GET)
	public List<ViewContratoUasgPorOrgao> getTabelaContratoUasgOrgao() {
		//log.info("SOMA DE CONTRATOS DO BD: "+this.contratoRepository.SomaPorOrgao());
		return this.contratoRepository.getTabelaContratoUasgOrgao();
	}

	@RequestMapping(value = "/query/soma/orgao", method = RequestMethod.GET)
	public List<ViewSomaPorOrgao> getSomaValorInicialPorOrgao() {
		//log.info("SOMA DE CONTRATOS DO BD: "+this.contratoRepository.SomaPorOrgao());
		return this.contratoRepository.getSomaValorInicialPorOrgao();
	}

	@RequestMapping(value = "/teste", method = RequestMethod.GET)
	public List<ViewT> getTeste() {
		//log.info("SOMA DE CONTRATOS DO BD: "+this.contratoRepository.SomaPorOrgao());
		return this.orgaoRepository.getOrgaoEUasg();
	}

}
