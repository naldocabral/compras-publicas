package br.com.mycode.controles;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import br.com.mycode.entidades.Aditivo;
import br.com.mycode.entidades.Cnae;
import br.com.mycode.entidades.Contrato;
import br.com.mycode.entidades.Fornecedor;
import br.com.mycode.entidades.Licitacao;
import br.com.mycode.entidades.ModalidadeLicitacao;
import br.com.mycode.entidades.Municipio;
import br.com.mycode.entidades.NaturezaJuridica;
import br.com.mycode.entidades.Orgao;
import br.com.mycode.entidades.PorteDeEmpresa;
import br.com.mycode.entidades.RamoDeNegocio;
import br.com.mycode.entidades.TipoDeContrato;
import br.com.mycode.entidades.Uasg;
import br.com.mycode.entidades.json.BodyAditivos;
import br.com.mycode.entidades.json.BodyCnaes;
import br.com.mycode.entidades.json.BodyContratos;
import br.com.mycode.entidades.json.BodyFornecedores;
import br.com.mycode.entidades.json.BodyLicitacoes;
import br.com.mycode.entidades.json.BodyModalidadeLicitacao;
import br.com.mycode.entidades.json.BodyMunicipios;
import br.com.mycode.entidades.json.BodyNaturezaJuridica;
import br.com.mycode.entidades.json.BodyOrgaos;
import br.com.mycode.entidades.json.BodyPorteDeEmpresa;
import br.com.mycode.entidades.json.BodyRamoDeNegocio;
import br.com.mycode.entidades.json.BodyTiposDeContrato;
import br.com.mycode.entidades.json.BodyUasgs;
import br.com.mycode.repositories.AditivoRepository;
import br.com.mycode.repositories.CnaeRepository;
import br.com.mycode.repositories.ContratoRepository;
import br.com.mycode.repositories.FornecedorRepository;
import br.com.mycode.repositories.LicitacaoRepository;
import br.com.mycode.repositories.ModalidadeLicitacaoRepository;
import br.com.mycode.repositories.MunicipioRepository;
import br.com.mycode.repositories.NaturezaJuridicaRepository;
import br.com.mycode.repositories.OrgaoRepository;
import br.com.mycode.repositories.PorteDeEmpresaRepository;
import br.com.mycode.repositories.RamoDeNegocioRepository;
import br.com.mycode.repositories.TipoDeContratoRepository;
import br.com.mycode.repositories.UasgRepository;

@Controller
public class AtualizaController {

	private static final Logger log = LoggerFactory
			.getLogger(AtualizaController.class);

	private ContratoRepository contratoRepository;
	private MunicipioRepository municipioRepository;
	private OrgaoRepository orgaoRepository;
	private TipoDeContratoRepository tipoDeContratoRepository;
	private RamoDeNegocioRepository ramoDeNegocioRepository;
	private PorteDeEmpresaRepository porteDeEmpresaRepository;
	private NaturezaJuridicaRepository naturezaJuridicaRepository;
	private ModalidadeLicitacaoRepository modalidadeLicitacaoRepository;
	private CnaeRepository cnaeRepository;
	private UasgRepository uasgRepository;
	private FornecedorRepository fornecedorRepository;
	private LicitacaoRepository licitacaoRepository;
	private AditivoRepository aditivoRepository;

	@Autowired
	public AtualizaController(ContratoRepository contratoRepository,
			MunicipioRepository municipioRepository,
			OrgaoRepository orgaoRepository,
			TipoDeContratoRepository tipoDeContratoRepository,
			RamoDeNegocioRepository ramoDeNegocioRepository,
			PorteDeEmpresaRepository porteDeEmpresaRepository,
			NaturezaJuridicaRepository naturezaJuridicaRepository,
			ModalidadeLicitacaoRepository modalidadeLicitacaoRepository,
			CnaeRepository cnaeRepository, UasgRepository uasgRepository,
			FornecedorRepository fornecedorRepository,
			LicitacaoRepository licitacaoRepository,
			AditivoRepository aditivoRepository) {
		this.contratoRepository = contratoRepository;
		this.municipioRepository = municipioRepository;
		this.orgaoRepository = orgaoRepository;
		this.tipoDeContratoRepository = tipoDeContratoRepository;
		this.ramoDeNegocioRepository = ramoDeNegocioRepository;
		this.porteDeEmpresaRepository = porteDeEmpresaRepository;
		this.naturezaJuridicaRepository = naturezaJuridicaRepository;
		this.modalidadeLicitacaoRepository = modalidadeLicitacaoRepository;
		this.cnaeRepository = cnaeRepository;
		this.uasgRepository = uasgRepository;
		this.fornecedorRepository = fornecedorRepository;
		this.licitacaoRepository = licitacaoRepository;
		this.aditivoRepository = aditivoRepository;
	}
	
	
	@RequestMapping(value = "/mapeamento", method = RequestMethod.GET)
	public void mapeamentoDeTabelas(){
		List<Orgao> orgaos = new ArrayList<Orgao>();
		orgaos = this.orgaoRepository.findAll();
		for (Orgao orgao : orgaos) {
			List<Uasg> uasgs = new ArrayList<Uasg>();
			uasgs = this.uasgRepository.findAll(orgao.getId());
			for (Uasg uasg : uasgs) {
				uasg.setOrgao(orgao);
				uasg.setMunicipio(this.municipioRepository.findOne(uasg
						.getId_municipio()));
				List<Fornecedor> fornecedores = new ArrayList<Fornecedor>();
				fornecedores = this.fornecedorRepository.findAll(uasg.getId());
				for (Fornecedor fornecedor : fornecedores) {
					fornecedor.setUasg(uasg);
					fornecedor.setMunicipio(this.municipioRepository
							.findOne(fornecedor.getId_municipio()));
					fornecedor.setCnae(this.cnaeRepository.findOne(fornecedor
							.getId_cnae()));
					fornecedor.setRamoDeNegocio(this.ramoDeNegocioRepository
							.findOne(fornecedor.getId_ramo_negocio()));
					fornecedor
							.setNaturezaJuridica(this.naturezaJuridicaRepository
									.findOne(fornecedor
											.getId_natureza_juridica()));
					fornecedor.setPorteDeEmpresa(this.porteDeEmpresaRepository
							.findOne(fornecedor.getId_porte_empresa()));
					List<Contrato> contratos = new ArrayList<Contrato>();
					contratos = this.contratoRepository.findAllByCnpj(fornecedor
							.getCnpj());
					for (Contrato contrato : contratos) {
						contrato.setFornecedor(fornecedor);
						contrato.setLicitacao(this.licitacaoRepository
								.findOne(contrato.getLicitacao_associada()));
						contrato.setModalidade_licitacao(this.modalidadeLicitacaoRepository
								.findOne(Long.parseLong(contrato
										.getModalidade_licit())));
						contrato.setTipoDeContrato(this.tipoDeContratoRepository
								.findOne(Long.parseLong(contrato
										.getTipo_contrato())));
						contrato.setUasg(this.uasgRepository.findOne(Long.parseLong(contrato.getUasgid())));
						List<Aditivo> aditivos = new ArrayList<Aditivo>();
						aditivos = this.aditivoRepository.findAll(contrato
								.getId());
						for (Aditivo aditivo : aditivos) {
							aditivo.setContrato(contrato);
							aditivo.setUasg(uasg);
						}
						contrato.setAditivos(aditivos);
					}
					fornecedor.setContratos(contratos);
				}
				uasg.setFornecedores(fornecedores);
				List<Contrato> contratos = new ArrayList<Contrato>();
				contratos = this.contratoRepository.findAllByUasg(uasg.getId());
				for(Contrato contrato: contratos){
					contrato.setFornecedor(this.fornecedorRepository.findOne(contrato.getCnpj_contratada()));
					contrato.setLicitacao(this.licitacaoRepository
							.findOne(contrato.getLicitacao_associada()));
					contrato.setModalidade_licitacao(this.modalidadeLicitacaoRepository
							.findOne(Long.parseLong(contrato
									.getModalidade_licit())));
					contrato.setTipoDeContrato(this.tipoDeContratoRepository
							.findOne(Long.parseLong(contrato
									.getTipo_contrato())));
					contrato.setUasg(this.uasgRepository.findOne(Long.parseLong(contrato.getUasgid())));
					List<Aditivo> aditivos = new ArrayList<Aditivo>();
					aditivos = this.aditivoRepository.findAll(contrato
							.getId());
					for (Aditivo aditivo : aditivos) {
						aditivo.setContrato(contrato);
						aditivo.setUasg(uasg);
					}
					contrato.setAditivos(aditivos);
					
				}
			uasg.setConstratos(contratos);
			List<Licitacao> licitacoes = new ArrayList<Licitacao>();
			licitacoes = this.licitacaoRepository.findAll(uasg.getId()+"");
			for(Licitacao licitacao: licitacoes){
				licitacao.setUasg(uasg);
				licitacao.setModalidade_licitacao(this.modalidadeLicitacaoRepository.findOne(Long.parseLong(licitacao.getModalidade())));
				licitacao.setContratos(this.contratoRepository.findAllByLicitacao(licitacao.getId()+""));
				
			}
			uasg.setLicitacoes(licitacoes);
			}
			
			this.orgaoRepository.saveAndFlush(orgao);
		}

	}

	@RequestMapping(value = "/atualizabd", method = RequestMethod.GET)
	public void atualizaBD() {

		this.atualizaOrgaos();
		this.atualizaMunicipios();
		this.atualizaTiposDeContrato();
		this.atualizaNaturezaJuridica();
		this.atualizaModalidadeLicitacao();
		this.atualizaRamosDeNegocio();
		this.atualizaPortesDeEmpresa();
		this.atualizaCnae();
		this.atualizaUasgs();
		this.atualizaFornecedores();
		this.atualizaLicitacoes();
		this.atualizaContratos();
		this.mapeamentoDeTabelas();
		this.log.info("##BANCO DE DADOS ATUALIZADO##");

		
	}

	public void atualizaMunicipios() {
		try {
			int count=0;
			RestTemplate restTemplate = new RestTemplate();
			String urlServico = "http://compras.dados.gov.br/fornecedores/v1/municipios.json?offset=";
			int offset = 0;
			String url = urlServico + offset;
			log.info("ATUALIZANDO MUNICIPIO - URL: " + url);
			ResponseEntity<BodyMunicipios> entidade = restTemplate
					.getForEntity(url, BodyMunicipios.class);
			
			while (count < entidade.getBody().getCount()) {
				for (Municipio municipio : entidade.getBody().getEmbedded()
						.getMunicipios()) {
					log.info("COUNT"+count);
					log.info(url);
					this.municipioRepository.save(municipio);
					count++;
				}
				offset += 500;
				url = urlServico + offset;
				entidade = restTemplate.getForEntity(url, BodyMunicipios.class);
			}
		} catch (RestClientException e) {
			log.info(e.getMessage());
		}

	}

	private void atualizaAditivos(Contrato contrato) {
		int count=0;
		try {
			RestTemplate restTemplate = new RestTemplate();
			String urlServico = "http://compras.dados.gov.br/contratos/id/contrato/"
					+ contrato.getId() + "/aditivos.json?offset=";
			int offset = 0;
			String url = urlServico + offset;
			log.info("ATUALIZANDO ATUALIZANDO ADITIVOS DE CONTRATO:"
					+ contrato.getId() + " - URL: " + url);
			ResponseEntity<BodyAditivos> entidade = restTemplate.getForEntity(
					url, BodyAditivos.class);

			while (count < entidade.getBody().getCount()) {
				for (Aditivo aditivo : entidade.getBody().getEmbedded()
						.getAditivos()) {
					this.aditivoRepository.save(aditivo);
					count++;
				}
				offset += 500;
				url = urlServico + offset;
				entidade = restTemplate.getForEntity(url, BodyAditivos.class);
			}
		} catch (RestClientException e) {
			log.info(e.getMessage());
		}

	}
	
	@RequestMapping(value = "/atualizacontratosbd", method = RequestMethod.GET)
	private void atualizaContratos() {
		int count=0;
		try {
			RestTemplate restTemplate = new RestTemplate();
			String urlServico = "http://compras.dados.gov.br/contratos/v1/contratos.json?assinatura_min=2010-01-01&offset=";
			int offset = 36293;
			String url = urlServico + offset;
			log.info("ATUALIZANDO CONTRATOS - URL: " + url);
			ResponseEntity<BodyContratos> entidade = restTemplate.getForEntity(
					url, BodyContratos.class);

			while (count< entidade.getBody().getCount()) {
				for (Contrato contrato : entidade.getBody().getEmbedded()
						.getContratos()) {
					this.atualizaAditivos(contrato);
					this.contratoRepository.save(contrato);
					count++;
				}
				offset += 500;
				url = urlServico + offset;
				entidade = restTemplate.getForEntity(url, BodyContratos.class);
			}
		} catch (RestClientException e) {
			log.info(e.getMessage());
		}

	}

	private void atualizaLicitacoes() {
		int count=0;
		try {
			RestTemplate restTemplate = new RestTemplate();
			String urlServico = "http://compras.dados.gov.br/licitacoes/v1/licitacoes.json?offset=";
			int offset = 0;
			String url = urlServico + offset;
			log.info("ATUALIZANDO LICITAÇÕES - URL: " + url);
			ResponseEntity<BodyLicitacoes> entidade = restTemplate
					.getForEntity(url, BodyLicitacoes.class);

			while (count< entidade.getBody().getCount()) {
				for (Licitacao licitacao : entidade.getBody().getEmbedded()
						.getLicitacoes()) {
					this.licitacaoRepository.save(licitacao);
					count++;
				}
				offset += 500;
				url = urlServico + offset;
				entidade = restTemplate.getForEntity(url, BodyLicitacoes.class);
			}
		} catch (RestClientException e) {
			log.info(e.getMessage());
		}

	}

	private void atualizaFornecedores() {
		int count=0;
		try {
			RestTemplate restTemplate = new RestTemplate();
			String urlServico = "http://compras.dados.gov.br/fornecedores/v1/fornecedores.json?offset=";
			int offset = 0;
			String url = urlServico + offset;
			log.info("ATUALIZANDO FORNECEDORES - URL: " + url);
			ResponseEntity<BodyFornecedores> entidade = restTemplate
					.getForEntity(url, BodyFornecedores.class);

			while (count< entidade.getBody().getCount()) {
				for (Fornecedor fornecedor : entidade.getBody().getEmbedded()
						.getFornecedores()) {
					this.fornecedorRepository.save(fornecedor);
					count++;
				}
				offset += 500;
				url = urlServico + offset;
				entidade = restTemplate.getForEntity(url,
						BodyFornecedores.class);
			}
		} catch (RestClientException e) {
			log.info(e.getMessage());
		}

	}
	
	@RequestMapping(value = "/corrigiFornecedores", method = RequestMethod.GET)
	private void corrigiFornecedores(){
			log.info("CORRIGINDO FORNECEDORES...");
		try{
			RestTemplate restTemplate = new RestTemplate();
			String urlServico = "http://compras.dados.gov.br/fornecedores/v1/fornecedores.json?cnpj=";
			String cnpj = "";
			List<Fornecedor> fornecedores = new ArrayList<Fornecedor>();
			fornecedores = this.fornecedorRepository.getAllNotNull();
			
			for(Fornecedor fornecedor: fornecedores){
				cnpj = fornecedor.getCnpj();
				String url = urlServico+cnpj;	
				ResponseEntity<BodyFornecedores> entidade = restTemplate
						.getForEntity(url, BodyFornecedores.class);
				String nome = entidade.getBody().getEmbedded().getFornecedores().get(0).getNome();
				fornecedor.setNome(nome);
				this.fornecedorRepository.saveAndFlush(fornecedor);
			}
			log.info("Fornecedores atualizados");
			
		}catch(Exception erro){
			erro.printStackTrace();
		}
		log.info("CORREÇÃO DE FORNECEDORES FINALIZADA.");
	}

	private void atualizaUasgs() {
		int count=0;
		try {
			RestTemplate restTemplate = new RestTemplate();
			String urlServico = "http://compras.dados.gov.br/licitacoes/v1/uasgs.json?offset=";
			int offset = 0;
			String url = urlServico + offset;
			log.info("ATUALIZANDO UASGS - URL: " + url);
			ResponseEntity<BodyUasgs> entidade = restTemplate.getForEntity(url,
					BodyUasgs.class);

			while (count< entidade.getBody().getCount()) {
				for (Uasg uasg : entidade.getBody().getEmbedded().getUasgs()) {
					this.uasgRepository.save(uasg);
					count++;
				}
				offset += 500;
				url = urlServico + offset;
				entidade = restTemplate.getForEntity(url, BodyUasgs.class);
			}
		} catch (RestClientException e) {
			log.info(e.getMessage());
		}

	}

	private void atualizaCnae() {
		int count=0;
		try {
			RestTemplate restTemplate = new RestTemplate();
			String urlServico = "http://compras.dados.gov.br/fornecedores/v1/cnaes.json?offset=";
			int offset = 0;
			String url = urlServico + offset;
			log.info("ATUALIZANDO CNAE - URL: " + url);
			ResponseEntity<BodyCnaes> entidade = restTemplate.getForEntity(url,
					BodyCnaes.class);

			while (count< entidade.getBody().getCount()) {
				for (Cnae cnae : entidade.getBody().getEmbedded().getCnaes()) {
					this.cnaeRepository.save(cnae);
					count++;
				}
				offset += 500;
				url = urlServico + offset;
				entidade = restTemplate.getForEntity(url, BodyCnaes.class);
			}
		} catch (RestClientException e) {
			log.info(e.getMessage());
		}

	}

	private void atualizaModalidadeLicitacao() {
		int count=0;
		try {
			RestTemplate restTemplate = new RestTemplate();
			String urlServico = "http://compras.dados.gov.br/licitacoes/v1/modalidades_licitacao.json?offset=";
			int offset = 0;
			String url = urlServico + offset;
			log.info("ATUALIZANDO MODALIDADE DE LICITAÇÃO - URL: " + url);
			ResponseEntity<BodyModalidadeLicitacao> entidade = restTemplate
					.getForEntity(url, BodyModalidadeLicitacao.class);

			while (count< entidade.getBody()
					.getCount()) {
				for (ModalidadeLicitacao modalidadeLicitacao : entidade
						.getBody().getEmbedded().getModalidadesLicitacao()) {
					this.modalidadeLicitacaoRepository
							.save(modalidadeLicitacao);
					count++;
				}
				offset += 500;
				url = urlServico + offset;
				entidade = restTemplate.getForEntity(url,
						BodyModalidadeLicitacao.class);
			}
		} catch (RestClientException e) {
			log.info(e.getMessage());
		}

	}

	private void atualizaNaturezaJuridica() {
		int count=0;
		try {
			RestTemplate restTemplate = new RestTemplate();
			String urlServico = "http://compras.dados.gov.br/fornecedores/v1/naturezas_juridicas.json?offset=";
			int offset = 0;
			String url = urlServico + offset;
			log.info("ATUALIZANDO NATUREZA JURIDICA - URL: " + url);
			ResponseEntity<BodyNaturezaJuridica> entidade = restTemplate
					.getForEntity(url, BodyNaturezaJuridica.class);

			while (count< entidade.getBody().getCount()) {
				for (NaturezaJuridica naturezaJuridica : entidade.getBody()
						.getEmbedded().getNaturezasJuridica()) {
					this.naturezaJuridicaRepository.save(naturezaJuridica);
					count++;
				}
				offset += 500;
				url = urlServico + offset;
				entidade = restTemplate.getForEntity(url,
						BodyNaturezaJuridica.class);
			}
		} catch (RestClientException e) {
			log.info(e.getMessage());
		}

	}

	private void atualizaPortesDeEmpresa() {
		int count=0;
		try {
			RestTemplate restTemplate = new RestTemplate();
			String urlServico = "http://compras.dados.gov.br/fornecedores/v1/portes_empresa.json?offset=";
			int offset = 0;
			String url = urlServico + offset;
			log.info("ATUALIZANDO PORTES DE EMPRESA - URL: " + url);
			ResponseEntity<BodyPorteDeEmpresa> entidade = restTemplate
					.getForEntity(url, BodyPorteDeEmpresa.class);

			while (count< entidade.getBody().getCount()) {
				for (PorteDeEmpresa porteDeEmpresa : entidade.getBody()
						.getEmbedded().getPortesDeEmpresa()) {
					this.porteDeEmpresaRepository.save(porteDeEmpresa);
					count++;
				}
				offset += 500;
				url = urlServico + offset;
				entidade = restTemplate.getForEntity(url,
						BodyPorteDeEmpresa.class);
			}
		} catch (RestClientException e) {
			log.info(e.getMessage());
		}

	}

	private void atualizaRamosDeNegocio() {
		int count=0;
		try {
			RestTemplate restTemplate = new RestTemplate();
			String urlServico = "http://compras.dados.gov.br/fornecedores/v1/ramos_negocio.json?offset=";
			int offset = 0;
			String url = urlServico + offset;
			log.info("ATUALIZANDO RAMOS DE NEGOCIO - URL: " + url);
			ResponseEntity<BodyRamoDeNegocio> entidade = restTemplate
					.getForEntity(url, BodyRamoDeNegocio.class);

			while (count< entidade.getBody().getCount()) {
				for (RamoDeNegocio ramoDeNegocio : entidade.getBody()
						.getEmbedded().getRamosDeNegocio()) {
					this.ramoDeNegocioRepository.save(ramoDeNegocio);
					count++;
				}
				offset += 500;
				url = urlServico + offset;
				entidade = restTemplate.getForEntity(url,
						BodyRamoDeNegocio.class);
			}
		} catch (RestClientException e) {
			log.info(e.getMessage());
		}

	}

	private void atualizaTiposDeContrato() {
		int count=0;
		try {
			RestTemplate restTemplate = new RestTemplate();
			String urlServico = "http://compras.dados.gov.br/contratos/v1/tipos_contrato.json?offset=";
			int offset = 0;
			String url = urlServico + offset;
			log.info("ATUALIZANDO TIPOS DE CONTRATO - URL: " + url);
			ResponseEntity<BodyTiposDeContrato> entidade = restTemplate
					.getForEntity(url, BodyTiposDeContrato.class);

			while (count< entidade.getBody().getCount()) {
				for (TipoDeContrato tipoDeContrato : entidade.getBody()
						.getEmbedded().getTiposDeContrato()) {
					this.tipoDeContratoRepository.save(tipoDeContrato);
					count++;
				}
				offset += 500;
				url = urlServico + offset;
				entidade = restTemplate.getForEntity(url,
						BodyTiposDeContrato.class);
			}
		} catch (RestClientException e) {
			log.info(e.getMessage());
		}

	}

	private void atualizaOrgaos() {
		int count=0;
		try {
			RestTemplate restTemplate = new RestTemplate();
			String urlServico = "http://compras.dados.gov.br/licitacoes/v1/orgaos.json?offset=";
			int offset = 0;
			String url = urlServico + offset;
			log.info("ATUALIZANDO ORGAOS - URL: " + url);
			ResponseEntity<BodyOrgaos> entidade = restTemplate.getForEntity(
					url, BodyOrgaos.class);

			while (count< entidade.getBody().getCount()) {
				for (Orgao orgao : entidade.getBody().getEmbedded().getOrgaos()) {
					this.orgaoRepository.save(orgao);
					count++;
				}
				offset += 500;
				url = urlServico + offset;
				entidade = restTemplate.getForEntity(url, BodyOrgaos.class);
			}
		} catch (RestClientException e) {
			log.info(e.getMessage());
		}

	}

}
